﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimalShelter
{

    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bdDataSet.Animals' table. You can move, or remove it, as needed.
            this.animalsTableAdapter.Fill(this.bdDataSet.Animals);
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void animalsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

        }


        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            Validate();
            animalsDataGridView.Update();
            animalsDataGridView.EndEdit();
            animalsTableAdapter.Update(bdDataSet.Animals);
            bdDataSet.AcceptChanges();
        
        }

        private void bindingNavigator1_RefreshItems(object sender, EventArgs e)
        {

        }

        private void animalsDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs anError)
        {
            MessageBox.Show("В поле 'Age' можно вводить только цифры");
            }

        private void animalsDataGridView_Click(object sender, EventArgs e)
        {
           
        }

        private void animalsDataGridView_SelectionChanged(object sender, EventArgs e)
        {
           
        }

        private void bindingNavigatorAddNewItem_Click_1(object sender, EventArgs e)
        {

        }

        private void файлToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void поступившиеЖивотныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnimalReport reportForm = new AnimalReport();
            reportForm.Show();
        }

        private void animalsDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        private void animalsDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
