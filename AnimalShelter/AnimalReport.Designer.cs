﻿namespace AnimalShelter
{
    partial class AnimalReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.AnimalsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataSet1 = new AnimalShelter.DataSet1();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.AnimalsTableAdapter = new AnimalShelter.DataSet1TableAdapters.AnimalsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.AnimalsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // AnimalsBindingSource
            // 
            this.AnimalsBindingSource.DataMember = "Animals";
            this.AnimalsBindingSource.DataSource = this.DataSet1;
            // 
            // DataSet1
            // 
            this.DataSet1.DataSetName = "DataSet1";
            this.DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.AnimalsBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "AnimalShelter.Report1.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(12, 12);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(649, 367);
            this.reportViewer1.TabIndex = 0;
            // 
            // AnimalsTableAdapter
            // 
            this.AnimalsTableAdapter.ClearBeforeFill = true;
            // 
            // AnimalReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 391);
            this.Controls.Add(this.reportViewer1);
            this.Name = "AnimalReport";
            this.Text = "Отчет: поступившие животные";
            this.Load += new System.EventHandler(this.AnimalReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AnimalsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource AnimalsBindingSource;
        private DataSet1 DataSet1;
        private DataSet1TableAdapters.AnimalsTableAdapter AnimalsTableAdapter;
    }
}